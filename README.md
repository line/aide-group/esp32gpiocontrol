# esp32gpiocontrol

ESP32 firmware providing a REST API for controlling the GPIO interface and higher functions

@aideAPI

Please refer to the usual ESP32/Arduino [language reference](https://www.arduino.cc/reference/en) for usual functions documentation.

## A tiny framework for ESP32 connected objects

This software package provides a minimal set of functionalities to develop a connected object using a [ESP32](https://en.wikipedia.org/wiki/ESP32) micro-controller.

In a nutshell:

- The object has a [GPIO](https://en.wikipedia.org/wiki/General-purpose_input/output) interface with digital and analog inputs and outputs and the capability to measure times, or connect to additional control board to drive, e.g., servomotors.
- It is connected to the (human or machine) user via a WiFi interface, as a web service, i.e., is driven using [URL](https://en.wikipedia.org/wiki/URL).
- It is programmed via an USB interface connected on a computer with an Arduino IDE development tool.

In order to ease such connected object development, we propose here:

- The EPS32 IDE environment and the esp32gpiocontrol software as [documented here](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/install_esp32.md).
- The [how-to documentation](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/usage_more.md) to develop specific connected objects.
- A GPIO control package also interesting as a development example, including the HTML interface for end user [documented here](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/usage_gpio.md):

![HTML web interface](https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol/setup_gpio_page.png "HTML web interface")


<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol'>https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol'>https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol'>softwareherirage.org</a>
- Version `1.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage


- Refer to <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/install_esp32.md'>install_esp32</a>, for the installation.,- Refer to <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/usage_gpio.md'>usage_gpio</a>, for the GPIO web service usage.,- Refer to <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/usage_more.md'>usage_more</a>, to get help implementing new functionalities.,

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- None

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Éric Pascual&nbsp; <big><a target='_blank' href='mailto:eric.g.pascual@gmail.com'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://twitter.com/ericpobot'>&#128463;</a></big>
- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
