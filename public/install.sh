#!/bin/bash

roof="`pwd`" ; roof="`dirname $roof`" ; roof="`dirname $roof`" ; roof="`basename $roof`"

# Detects if in production mode

if [ "$roof" = "node_modules" ]
then
  d=../../../src

  # Installs all default esp32gpio firmware files
  for f in .makefile0.inc handler.hpp setup.cpp loop.cpp gpio_digital_timing.cpp
  do
   if [ -f $d/$f ]
   then diff -q `pwd`/$f $d/$f ; diff `pwd`/$f $d/$f
   else cp `pwd`/$f $d/$f
   fi
  done
  
  # Installs the makefile include line
  if grep .makefile0.inc $d/makefile >/dev/null ; then ok= 
  else 
    cp $d/makefile $d/makefile~ 
    (head -3 $d/makefile~ ; echo 'include ./.makefile0.inc' ; echo ; tail -n +4 $d/makefile~) > $d/makefile
  fi
  
  # Installs the default .ino file
  n=`pwd` ; n="`dirname $n`" ; n="`dirname $n`" ; n="`dirname $n`" ; n="`basename $n`" 
  if [ \! -f $d/$n.ino ] 
  then cp `pwd`/esp32gpiocontrol.ino $d/$n.ino
  fi
fi
