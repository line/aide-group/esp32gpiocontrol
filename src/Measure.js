/** Implements a repeated mesaure mechanism.
 * 
 * - Allows to accumulate different measures of the same value to obtain basic statistics.
 *
 * - Typical usage:
 * ```
 *  // Creates a measure
 *  var measure = new Measure();
 *  // Adds 1000 random values is this example
 *  for(i = 0; i < 1000; i++)
 *     measure.add(Math.random());
 *  // Dumps the result
 *  console.log(measure.asString());
 * ```
 *
 * @param name The measure name, if any.
 * @class
 */
const Measure = function(name = "") {
  /** The measure name. */
  this.name = name;
  /** The measures count. */
  this.count = 0;
  /** The mean value. */
  this.mean = null;
  var m1 = null;
  /** The mean value standard-deviation. */
  this.stdev = null;
  var m2 = null;
  /** The minimal value. */
  this.min = null;
  /** The maximal value. */
  this.max = null;
  /** Clears all measures of the value. */
  this.clear = function() {
    this.count = 0;
    this.mean = this.stdev = this.min = this.max = null;
  };
  /** Adds a measure of the value. 
   * @param value The measure value to add.
   */
  this.add = function(value) {
    this.count++;
    m1 = ((m1 == null) ? 0 : m1) + value;
    this.mean = m1 / this.count;
    m2 = ((m2 == null) ? 0 : m2) + value * value;
    this.stdev = m2 / this.count - this.mean * this.mean, this.stdev = this.stdev > 0 ? Math.sqrt(this.stdev) : 0;
    this.min = this.min == null || this.min > value ? value : this.min;
    this.max = this.max == null || this.max < value ? value : this.max;
  };
  /** Returns the measure as a readable string. 
   * @param digits The number of digits using fixed-point notation.
   * @return A string of the form `mean +- stdev [min .. max] #count`
   */
  this.asString = function(digits = 3) {
    return (this.name == "" ? "" : (this.name + ": ")) + (this.count == 0 ? "undefined" : this.count == 1 ? Number(this.mean).toFixed(digits) : "" + Number(this.mean).toFixed(digits) + " +- " + Number(this.stdev).toFixed(digits) + " [" + Number(this.min).toFixed(digits) + " .. " + Number(this.max).toFixed(digits) + "] #" + this.count);
  };
};

// module.exports = Measure;
