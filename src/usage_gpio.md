# Using the GPIO web interface

- Installation is [documented](./install_esp32.md) here.

## General low-level functions

- 'curl -X GET http://$IP:80` => `{ "version": "$firmware-version" }`
  - allows to verify that the web service is started
- `curl -X POST http://$IP:80/restart` 
  - allows to restart the web service

## Fallback route to control the GPIO

Because WebServer.h seems to have a bug a fallback mechanism is implemented on a simpler route

- `curl -X POST -d "index=$index" http://$IP:80/gpio` for digital input
- `curl -X POST -d "index=$index&value=(0|1)" http://$IP:80/gpio` for digital output
- `curl -X POST -d "index=$index&mode=analog&range=(0|1|2|3)" http://$IP:80/gpio` for analog input
- `curl -X POST -d "index=$index&mode=analog&value=$value" http://$IP:80/gpio` for analog output
- `curl -X POST -d "index=$index&action=(start|stop|get)&mode=(change|rising|falling)" http://$IP:80/gpio` for timing control

## Digital input or output

- `curl -X GET http://$IP:80/gpio/digital/$index` => `{ "action": \"gpio_digital_get_value\", "index": $index, "value": $value, "time": $time}`
  - allows to get a digital input value
- `curl -X PUT -d "value=$value" http://$IP:80/gpio/digital/$index` => `{ "action": \"gpio_digital_put_value\", "index": $index, "value": $value, "time": $time}`
  - allows to set a digital output value
- `curl -X POST -d "action=(start|stop|get)&mode=(change|rising|falling)" http://$IP:80/gpio/digital/$index` => `{ "action": \"gpio_digital_post_value\", "index": $index, "action": $action, "mode": $mode, "time": $time, "last-occurrence-time": $last-occurrence-time}`
  - allows to measure the time of the last change of the input digital signal in two steps
    - Starting the mechanism in the desired mode
    - Reading the last occurence value: last-occurrence-time is 0 if no occurrence.

Arguments:

- The index is the GPIO index pin number between 0 and 31, 0 by default.
- The value is the 0 (false) or 1 (true) Boolean value.
- The time of the request, given in number of milliseconds passed since the program started.

- The post action:
  - `start`: Starts the time occurence measure
  - `stop`: Stops the time occurence measure, and reset the occurrence time
  - `get`: Gets the last time occurrence of the signal (default)
- The post action mode:
  - `change`: to trigger the interrupt whenever the pin changes value.
  - `rising`: to trigger when the pin goes from low to high.
  - `falling`: to trigger when the pin goes from high to low.
- The last occurrence time:
  - The number of milliseconds passed since the program started, where the last occurence of the input signal change has been register (only one occurrence is stored).

## Analog input or output

  - `curl -X GET http://$IP:80/gpio/analog/$index?range=(0|1|2|3)` => `{ "action": \"gpio_analog_get_value\", "index": $index, "range": $range, "value": $value, "time": $time}`
    - allows to get an analog input value, at a given time (in number of milliseconds passed since the program started).
  - `curl -X PUT -d "value=$value" http://$IP:80/gpio/analog/$index` => `{ "action": \"gpio_analog_put_value\", "index": $index, "value": $value, "time": $time}`
    - allows to set an analog output value.

Arguments:

- The index is the GPIO index pin number, between 1 and 2 for output and 0 to 16 for input, 0 by default.
- The value.
  - On input 12bits analog value between 0 and 4095.
  - On output 8 bits analog value between 0 and 255.
- The input range.
  - 0: 11dB attenuation gives full-scale voltage 3.3V, linear between 100 and 2450mV, default.
  - 1: 6dB attenuation gives full-scale voltage 2.2V, linear between 100 and 1750mV.
  - 2: 2.5dB attenuation gives full-scale voltage 1.5V, linear between 100 and 1250mV.
  - 3: 0dB attenuation gives full-scale voltage 1.1V, linear between 100 and 950mV.
- The time of the request, given in number of milliseconds passed since the program started.
