// Wifi network connection credentials

std::map <String, String> wifis = {
  // {"SSID_1", "PASSWORD_1"},
  // ../..
  // {"SSID_N", "PASSWORD_N"}
};

// 1/ Copy this file to wifi.h : cp wifi-configure.h wifi.h
// 2/ Edit the wifi.h file adding the correct SSID and PASSWORD for all possible networks
// 3/ Do NOT add wifi.h to the git repository

/* The data structure allows to define several wifi access points, tested in sequence, 
  allowing the circuit to connect in different situation, without redfining the firmware.

  The syntax is given above, ssid and pasword (in a visible form, thus do NOT publish the file)
  are between ``"´´ quotes, and enclosed between braces.
  */


