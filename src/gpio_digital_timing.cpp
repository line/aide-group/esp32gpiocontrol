#include "handler.hpp"
#include <set>

//
// Digital timing implementation
//

unsigned long int handle_gpio_digital_times[MAX_GPIO_DIGITAL_INPUT] =
{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

void IRAM_ATTR handle_gpio_digital_interrupt_handler_0()
{
  verbose("gpio_digital_interrupt_00");
  handle_gpio_digital_times[0] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_1()
{
  verbose("gpio_digital_interrupt_01");
  handle_gpio_digital_times[1] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_2()
{
  verbose("gpio_digital_interrupt_02");
  handle_gpio_digital_times[2] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_3()
{
  verbose("gpio_digital_interrupt_03");
  handle_gpio_digital_times[3] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_4()
{
  verbose("gpio_digital_interrupt_04");
  handle_gpio_digital_times[4] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_5()
{
  verbose("gpio_digital_interrupt_05");
  handle_gpio_digital_times[5] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_6()
{
  verbose("gpio_digital_interrupt_06");
  handle_gpio_digital_times[6] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_7()
{
  verbose("gpio_digital_interrupt_07");
  handle_gpio_digital_times[7] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_8()
{
  verbose("gpio_digital_interrupt_08");
  handle_gpio_digital_times[8] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_9()
{
  verbose("gpio_digital_interrupt_09");
  handle_gpio_digital_times[9] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_10()
{
  verbose("gpio_digital_interrupt_10");
  handle_gpio_digital_times[10] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_11()
{
  verbose("gpio_digital_interrupt_11");
  handle_gpio_digital_times[11] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_12()
{
  verbose("gpio_digital_interrupt_12");
  handle_gpio_digital_times[12] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_13()
{
  verbose("gpio_digital_interrupt_13");
  handle_gpio_digital_times[13] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_14()
{
  verbose("gpio_digital_interrupt_14");
  handle_gpio_digital_times[14] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_15()
{
  verbose("gpio_digital_interrupt_15");
  handle_gpio_digital_times[15] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_16()
{
  verbose("gpio_digital_interrupt_16");
  handle_gpio_digital_times[16] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_17()
{
  verbose("gpio_digital_interrupt_17");
  handle_gpio_digital_times[17] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_18()
{
  verbose("gpio_digital_interrupt_18");
  handle_gpio_digital_times[18] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_19()
{
  verbose("gpio_digital_interrupt_19");
  handle_gpio_digital_times[19] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_20()
{
  verbose("gpio_digital_interrupt_20");
  handle_gpio_digital_times[20] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_21()
{
  verbose("gpio_digital_interrupt_21");
  handle_gpio_digital_times[21] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_22()
{
  verbose("gpio_digital_interrupt_22");
  handle_gpio_digital_times[22] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_23()
{
  verbose("gpio_digital_interrupt_23");
  handle_gpio_digital_times[23] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_24()
{
  verbose("gpio_digital_interrupt_24");
  handle_gpio_digital_times[24] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_25()
{
  verbose("gpio_digital_interrupt_25");
  handle_gpio_digital_times[25] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_26()
{
  verbose("gpio_digital_interrupt_26");
  handle_gpio_digital_times[26] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_27()
{
  verbose("gpio_digital_interrupt_27");
  handle_gpio_digital_times[27] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_28()
{
  verbose("gpio_digital_interrupt_28");
  handle_gpio_digital_times[28] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_29()
{
  verbose("gpio_digital_interrupt_29");
  handle_gpio_digital_times[29] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_30()
{
  verbose("gpio_digital_interrupt_30");
  handle_gpio_digital_times[30] = millis();
}
void IRAM_ATTR handle_gpio_digital_interrupt_handler_31()
{
  verbose("gpio_digital_interrupt_31");
  handle_gpio_digital_times[31] = millis();
}
typedef void (*handler)(void);
handler handle_gpio_digital_interrupt_handlers[MAX_GPIO_DIGITAL_INPUT] = {
  handle_gpio_digital_interrupt_handler_0,
  handle_gpio_digital_interrupt_handler_1,
  handle_gpio_digital_interrupt_handler_2,
  handle_gpio_digital_interrupt_handler_3,
  handle_gpio_digital_interrupt_handler_4,
  handle_gpio_digital_interrupt_handler_5,
  handle_gpio_digital_interrupt_handler_6,
  handle_gpio_digital_interrupt_handler_7,
  handle_gpio_digital_interrupt_handler_8,
  handle_gpio_digital_interrupt_handler_9,
  handle_gpio_digital_interrupt_handler_10,
  handle_gpio_digital_interrupt_handler_11,
  handle_gpio_digital_interrupt_handler_12,
  handle_gpio_digital_interrupt_handler_13,
  handle_gpio_digital_interrupt_handler_14,
  handle_gpio_digital_interrupt_handler_15,
  handle_gpio_digital_interrupt_handler_16,
  handle_gpio_digital_interrupt_handler_17,
  handle_gpio_digital_interrupt_handler_18,
  handle_gpio_digital_interrupt_handler_19,
  handle_gpio_digital_interrupt_handler_20,
  handle_gpio_digital_interrupt_handler_21,
  handle_gpio_digital_interrupt_handler_22,
  handle_gpio_digital_interrupt_handler_23,
  handle_gpio_digital_interrupt_handler_24,
  handle_gpio_digital_interrupt_handler_25,
  handle_gpio_digital_interrupt_handler_26,
  handle_gpio_digital_interrupt_handler_27,
  handle_gpio_digital_interrupt_handler_28,
  handle_gpio_digital_interrupt_handler_29,
  handle_gpio_digital_interrupt_handler_30,
  handle_gpio_digital_interrupt_handler_31
};
void gpio_digital_timing_start(unsigned int index, unsigned int mode)
{
  if(MAX_GPIO_DIGITAL_INPUT != 32) {
    Serial.println("Error: Spurious MAX_GPIO_DIGITAL_INPUT value, this will lead to fatal errors !");
  }
  if(index < MAX_GPIO_DIGITAL_INPUT) {
    pinMode(index, INPUT);
    noInterrupts();
    attachInterrupt(digitalPinToInterrupt(index), handle_gpio_digital_interrupt_handlers[index], mode);
    interrupts();
    handle_gpio_digital_times[index] = 0;
  }
}
void gpio_digital_timing_stop(unsigned int index)
{
  if(index < MAX_GPIO_DIGITAL_INPUT) {
    detachInterrupt(digitalPinToInterrupt(index));
  }
}
unsigned int gpio_digital_timing_get(unsigned int index)
{
  return index < MAX_GPIO_DIGITAL_INPUT ? handle_gpio_digital_times[index] : 0;
}
//
// Software implemented event mechanism
//

struct setInterval_Event {
  unsigned int next_time;
  handler handler_routine;
  unsigned int delay;
  unsigned int count;
  bool operator < (setInterval_Event const& event) const {
    return next_time < event.next_time || (next_time == event.next_time && handler_routine < event.handler_routine);
  }
};

std::set < setInterval_Event > setInterval_events;
unsigned int setInterval_next_time = -1;

/* // used for debug only
 *  void setInterval_dump(String from)
 *  {
 *  sprintf(printf_buffer, "{from: %s now_time: %d events: {", from.c_str(), millis());
 *  Serial.println(printf_buffer);
 *  for(auto it = setInterval_events.begin(); it != setInterval_events.end(); it++) {
 *   sprintf(printf_buffer, "  {next_time: %d delay: %d count: %d handler_routine: %d}",
 *     it->next_time, it->delay, it->count, it->handler_routine);
 *   Serial.println(printf_buffer);
 *  }
 *  Serial.println("}}");
 *  }
 */

void setInterval(handler handler_routine, unsigned int delay, unsigned int count)
{
  if(delay == 0) {
    // Deletes all events with this handler_routine
    for(auto it = setInterval_events.begin(); it != setInterval_events.end(); it++) {
      if(it->handler_routine == handler_routine) {
        setInterval_events.erase(it);
      }
    }
  } else {
    // Inserts the corresponding event
    setInterval_Event event;
    event.next_time = millis() + delay, event.handler_routine = handler_routine, event.delay = delay, event.count = count;
    setInterval_events.insert(event);
  }
  // Registers next event time
  setInterval_next_time = setInterval_events.size() == 0 ? -1 : setInterval_events.begin()->next_time;
  // - setInterval_dump("setInterval"); // was used for debug
}
void setInterval_manage()
{
  // Detects if it is time to fire an event
  if(setInterval_next_time != -1 && setInterval_next_time < millis()) {
    auto it = setInterval_events.begin();
    // Calls the handler routine
    it->handler_routine();
    // Replaces the event by a new one if appropriate
    setInterval_Event event = *it;
    setInterval_events.erase(it);
    event.count = event.count == -1 ? -1 : event.count - 1;
    if(event.count != 0) {
      event.next_time += event.delay;
      setInterval_events.insert(event);
    }
    // Registers next event time
    setInterval_next_time = setInterval_events.size() == 0 ? -1 : setInterval_events.begin()->next_time;
    // - setInterval_dump("setInterval"); // was used for debug
  }
}
