@aideAPI

Please refer to the usual ESP32/Arduino [language reference](https://www.arduino.cc/reference/en) for usual functions documentation.

## A tiny framework for ESP32 connected objects

This software package provides a minimal set of functionalities to develop a connected object using a [ESP32](https://en.wikipedia.org/wiki/ESP32) micro-controller.

In a nutshell:

- The object has a [GPIO](https://en.wikipedia.org/wiki/General-purpose_input/output) interface with digital and analog inputs and outputs and the capability to measure times, or connect to additional control board to drive, e.g., servomotors.
- It is connected to the (human or machine) user via a WiFi interface, as a web service, i.e., is driven using [URL](https://en.wikipedia.org/wiki/URL).
- It is programmed via an USB interface connected on a computer with an Arduino IDE development tool.

In order to ease such connected object development, we propose here:

- The EPS32 IDE environment and the esp32gpiocontrol software as [documented here](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/install_esp32.md).
- The [how-to documentation](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/usage_more.md) to develop specific connected objects.
- A GPIO control package also interesting as a development example, including the HTML interface for end user [documented here](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/usage_gpio.md):

![HTML web interface](https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol/setup_gpio_page.png "HTML web interface")

