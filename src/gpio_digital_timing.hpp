/**
 * @function answer
 * @static
 * @description Returns a response to the web service.
 * @param {bool} noerror If true, uses the HTTP 200 code (no error), else the HTTP 400 code (bad request).
 * @param {string} message The message, a string format à-la printf with more parameters if used.
 * @param {...anything} parameters Any message parameters.
 */

/**
 * @function gpio_digital_timing_start
 * @static
 * @description Starts the digital timing observation.
 * @param {uint} index The given pin index.
 * @param {uint} mode The transition `CHANGE|FALLING|RISING` mode.
 */

/**
 * @function gpio_digital_timing_stop
 * @static
 * @description Stops the digital timing observation.
 * @param {uint} index The given pin index.
 */

/**
 * @function gpio_digital_timing_get
 * @static
 * @description Gets the last digital timing observation.
 * @param {uint} index The given pin index.
 * @return The last observation time in millisconds from the chip boot.
 */

/**
 * @function setInterval
 * @static
 * @description Calls a handler at specified intervals (in milliseconds).
 * - Functionnalities:
 *   - _Periodic samplig_: This corresponds to the `setInterval(handler, delay)` javascript mechanism.
 *   - _Sampling stopping_: The `setInterval(handler, 0)` is equivalent to `cleaInterval(handler)` javascript mechanism.
 *   - _Timeout handler call_: The `setInterval(handler, delay, 1)` is equivalent to `setTimeout(handler)` javascript mechanism.
 * - Requirement: In order the mechanism to be used the `setInterval_manage()` function has to be added to the processor loop, as [examplified here](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/loop.cpp).
 * - Precision:
 *   - The present [`loop()`](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/loop.cpp)
 *     - is called about every `1` millisecond (without any action to performed), while performing a `Serial.println` requires about 2 to 3 milliseconds.
 *     - when implementing this `setInterval` management and the webservice client handler
 *     - while it is delayed by the web service handle routine.
 *   - The `millis()` returns the time passed since the board began running the current program.
 * @param {callback} handler The `void handler()` routine to be called.
 * @param {unit} delay The intervals (in milliseconds) on how often to execute the code, `0` to stop the sampling.
 * - This is performs in "best effort" mode, i.e., as soon as possible.
 * - The 1st call is performed after the given delay.
 * @param {uint} count The maximal number of calls, `-1` for endless calls.
 */
