# Programming higher level functions

## Installing a framework files

- Install the EPS32 IDE environment and the esp32gpiocontrol software as [documented here](./install_esp32.md).

- Choose a package  `$name` and create a directory with this name in the `Arduino/tools` directory
- Create a minimal `$name.ino` file for the Arduino IDE builder, usually a [one line comment file](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/esp32gpiocontrol.ino) with the same name as the package and its directory.
- Copy from `Arduino/tools/esp32gpiocontrol` the `setup.cpp loop.cpp wifi.h handler.hpp gpio_digital_timing.cpp` in the current package directory.
  - [`setup.cpp`](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/setup.cpp) contains the package initialisation and the `setup_service()` and `setup_init()` routnes have to adapted as explained in the next subsection, othewise the file is not to be changed.
  - [`loop.cpp`](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/loop.cpp) implements the REST web service and has not be changed in common usage.
  - `wifi.h` contains the wifi configurations as [documented here]( https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/wifi-configure.h) and is to be updated.
  - [`handler.hpp`](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/handler.hpp) is the header of the specific configuration files and contains all framework definitions.
  - [`gpio_digital_timing.cpp`](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/gpio_digital_timing.cpp) implements the [`gpio_digital_timing` functions](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/gpio_digital_timing.cpp) and is not to be changed.
- Create a `setup_$name.cpp` file and implements your the required functionalities and web service routes as explained now.

## Creating a setup for the given functionalities

- Creates a specific setup file from the [given example](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/setup_example.cpp.txt), i.e., in `gpiocontrol`:
  - `cp setup_example.cpp.txt setup_$name.cpp`
  - Instantiate the `setup_$name_service()` routine with any desired service.
  - Instantiate the `setup_$name_start()` routine with any desired mechanisms to start, after setup.

- In `[setup.cpp](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/setup.cpp)`: 
 - Add the `set_$name()_service` routine both 
   - in the declaration part and 
   - in the `setup_service()` routine following the `setup_gpio` example.
 - Add (if appropriate) the `setup_$name_init()` routine also in declaration part and in the `setup_init()` routine.

## Using implementing timing and sampling functions

This framework implements functions to measure a delay on any gpio input pin. 

- The [`gpio_digital_timing_get()`](https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol/global.html) function returns the last oberved time. 
- The [`gpio_digital_timing_start()`](https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol/global.html) function enables this mechanims.

This framework implements functions to sample a handler or start it after a given timeout.
 - The [`setInterval()`](https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol/global.html) function enables this mechanism.
 - This covers the usual `setInterval`, `clearInterval` and `setTimeout` javascript functionnalities.

## Using a HTML panel to control the device

In order to dialog with the connected object via a web page:

- Add the [makefile](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/.makefile0.inc) to the makefile in order to generates a `.hpp` from the `.html` file.
- In the `setup_$name.cpp` file add a handle routine and simple get service route, of this form:
```
void handle_$name_page()
{
#include "setup_$name_page.hpp"
  server->send(200, "text/html", setup_$name_page);
}
// ../..
void setup_$name_service()
{
// ../..
  server->on("/$name/index.html", HTTP_GET, handle_$name_page);
// ../..
}
```
- Creates a `setup_$name_page.html` page, from for instance [this example](https://gitlab.inria.fr/line/aide-group/esp32galileo/-/blob/master/src/setup_galileo_page.html) to define the user interface.

This allows to one page minimal HTML service without requiring any file management.

## Useful IDE commands

The following commands are of current usage 

- _Loading a firmware source bundle_ `File => Open` and navigate to the proper `Arduino/tools/$name` directory to pen the corresponding `$name.ino` file
- _Opening the debug/develop console_ `Tools => Serial Monitor` to open the console, the chip being connected to desktop computer via a USB cable
  - The `/dev/ttyUSB0` or equivalent port is confirgured in the `Tools => Port =>` menu
- _Compiling and loading_:
  - The `Sketch => Verify/Compile` (`CTRL+R` as shortcut) menu command allows to prepare the firmware.
  - The `Sketch => Upload Using Programmer` (`CTRL+U` as shortcut) menu command allows to prepare and load the firmware.

The connection to the _Serial Monitor_ is useful for:

- Uploading a new firmare, checking also that the circuit is ok
- Read the circuit MAC address and IP address on the used wifi network
- Gets some verbose messages printed by the code

## Useful documentation

- [ESP32 hardware pins documentation](https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/pinouts)
- [ESP32 basic routines (similar to arduino)](https://www.arduino.cc/reference)
- [ESP32 tutotial](https://lastminuteengineers.com/esp32-arduino-ide-tutorial)
