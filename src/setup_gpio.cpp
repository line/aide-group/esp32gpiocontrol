// Implements the general GPIO direct digital and analog control including input timing detection

#include "handler.hpp"

#include <analogWrite.h>

//
// GPIO general service handlers
//

// Firmware version query

void handle_version_query()
{
  answer(true, "{\"status\": \"running\", \"version\": \"%s\"}", firmware_version);
}
// ESP32 reboot request

void handle_restart()
{
  Serial.println("Restart required");
  server->send(200, "text/plain", "Restart required, see you soon :)");
  delay(500);
  ESP.restart();
}
// ESP32 GPIO digital get and set value

void handle_gpio_digital_get_value()
{
  unsigned int index = server->hasArg("index") ? server->arg("index").toInt() : server->pathArg(0).toInt();
  pinMode(index, INPUT);
  unsigned int value = digitalRead(index);
  answer(true, "{\"route\": \"gpio_digital_get_value\", \"index\": %d, \"value\": %d, \"time\": %.3f}", index, value, fmillis());
}
void handle_gpio_digital_put_value()
{
  unsigned int index = server->hasArg("index") ? server->arg("index").toInt() : server->pathArg(0).toInt();
  unsigned int value = server->arg("value").toInt();
  pinMode(index, OUTPUT);
  digitalWrite(index, value == 0 ? 0 : 1);
  value = digitalRead(index);
  answer(true, "{\"route\": \"gpio_digital_put_value\", \"index\": %d, \"value\": %d, \"time\": %.3f}", index, value, fmillis());
}
// ESP32 GPIO digital time measurement mechanism
void handle_gpio_digital_post_value()
{
  unsigned int index = server->hasArg("index") ? server->arg("index").toInt() : server->pathArg(0).toInt();
  String action = server->arg("action");
  String mode = server->arg("mode");
  if(index < MAX_GPIO_DIGITAL_INPUT) {
    if(action == "start") {
      gpio_digital_timing_start(index, mode == "rising" ? RISING : mode == "falling" ? FALLING : CHANGE);
    } else if(action == "stop") {
      gpio_digital_timing_stop(index);
    }
    answer(true, "{\"route\": \"gpio_digital_post_value\", \"index\": %d, \"action\": \"%s\", \"mode\": \"%s\", \"time\": %.3f, \"last-interrupt-times\": %d}", index, action.c_str(), mode.c_str(), fmillis(), gpio_digital_timing_get(index));
  } else {
    answer(false, "{\"route\": \"gpio_digital_post_value\", \"index\": %d, \"action\": \"%s\", \"mode\": \"%s\", \"time\": %.3f, \"error\": \"Incorrect index value\"}", index, action.c_str(), mode.c_str(), fmillis());
  }
}
// ESP32 GPIO analog get and set value

void handle_gpio_analog_get_value()
{
  unsigned int index = server->hasArg("index") ? server->arg("index").toInt() : server->pathArg(0).toInt();
  unsigned int range = server->hasArg("range") ? server->arg("range").toInt() : 3;
  analogReadResolution(12);
  analogSetAttenuation(range == 3 ? ADC_0db : range == 2 ? ADC_2_5db : range == 1 ? ADC_6db : ADC_11db);
  unsigned int value = analogRead(index);
  answer(true, "{\"route\": \"gpio_analog_get_value\", \"index\": %d, , \"range\": %d, \"value\": %d, \"time\": %.3f}", index, range, value, fmillis());
}
void handle_gpio_analog_put_value()
{
  unsigned int index = server->hasArg("index") ? server->arg("index").toInt() : server->pathArg(0).toInt();
  unsigned int value = server->arg("value").toInt();
  analogWrite(index, value, 255);
  answer(true, "{\"route\": \"gpio_analog_set_value\", \"index\": %d, \"value\": %d, \"time\": %.3f}", index, value, fmillis());
}
void handle_gpio_page()
{
#include "setup_gpio_page.hpp"
  server->send(200, "text/html", setup_gpio_page);
}
//
// Fallback service because of a bug in WebServer.h
//

void handle_gpio()
{
  if(server->hasArg("index")) {
    if(server->hasArg("action")) {
      handle_gpio_digital_post_value();
    } else {
      String mode = server->hasArg("mode") ? server->arg("mode") : "digital";
      if(server->hasArg("value")) {
        if(mode == "analog") {
          handle_gpio_analog_put_value();
        } else {
          handle_gpio_digital_put_value();
        }
      } else {
        if(mode == "analog") {
          handle_gpio_analog_get_value();
        } else {
          handle_gpio_digital_get_value();
        }
      }
    }
  } else {
    answer(false, "{\"route\": \"gpio\", \"error\": \"undefined index\"}", "");
  }
}
//
// Service configuration
//

void setup_gpio_service()
{
  // General commands
  server->on("/", HTTP_GET, handle_version_query);
  server->on("/restart", HTTP_POST, handle_restart);

  // GPIO HTML interface
  server->on("/gpio/index.html", HTTP_GET, handle_gpio_page);

  // GPIO digital and analog I/O commands
  server->on("/gpio/digital/{}", HTTP_GET, handle_gpio_digital_get_value);
  server->on("/gpio/digital/{}", HTTP_PUT, handle_gpio_digital_put_value);
  server->on("/gpio/digital/{}", HTTP_POST, handle_gpio_digital_post_value);
  server->on("/gpio/analog/{}", HTTP_GET, handle_gpio_analog_get_value);
  server->on("/gpio/analog/{}", HTTP_PUT, handle_gpio_analog_put_value);

  // GPIO fallback mechanism because of a bug
  server->on("/gpio", HTTP_POST, handle_gpio);
}
