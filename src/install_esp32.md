# Installing a the EPS32 development and firmware configuration 

## Installing the EPS32 IDE environment

The [Arduino IDE](https://www.arduino.cc/en/main/software) is used for EPS32 development and firmware configuration 

- To download and install the [Arduino IDE](https://www.arduino.cc/en/main/software) either consider this [link](https://www.arduino.cc/en/main/software) or use your package installer, e.g., 
  - _sudo dnf install arduino_
  - Start arduino
    - Following the internal instructions, adds your login account the _dialout_ and _lock_ groups in order to access _/dev/ttyUSB0_ devices
      - This is done automatically in `/etc/group` but you must enter the root password

- Installing the ESP32 board support and additional library
  - Start arduino
  - Open File => Preferences => Settings
    - Enter "https://dl.espressif.com/dl/package_esp32_index.json" into Additional Board Manager URLs field. 
    - You can add multiple URLs, separating them with commas.
  - Open Tools => Boards => Board Manager window
    - Search "ESP32"in the upper bar
    - Install ESP32 by Espressif Systems
  - In Tools  => Boards 
    - Select "ESP32 Dev Module"
    - Install
  - In Sketch => Include Library => Manage Libraries
    - Search "analogWrite" and click on Install
    - Select any other library you need:
      - "Adafruit PWM servo driver library" and click on Install
  - Manually install the patched "Adafruit VS1053 library"
    - Install "SD" via Manage Libraries
    - Download https://github.com/danclarke/Adafruit_VS1053_Library unzip and install in Arduino/libraries/Adafruit_VS1053_Library

- A few remarks
  - One may have to `pip install pyserial` if an _Error: No module named 'serial'_ occurs.

## Installing the esp32gpiocontrol software

- Installing the [esp32gpiocontrol](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol). In the Arduino sketchbook directory, we (i) create a 3rd party contents directoty etc, (ii) clone the esp32gpiocontrol firmware, and (iii) creat ethe proper link in order the IDE to manage it:
  - _cd Arduino_
  - _mkdir -p etc_
  - _cd Arduino/etc ; git clone https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol.git_
  - _cd Arduino/tools ; ln -s ../etc/esp32gpiocontrol/src esp32gpiocontrol

- Compiling and loading
  - Connect the ESP32 card via the USB cable
  - Start Arduino
  - In Tools => Port select /dev/ttyUSB0
  - In File => Open, browse "tools/esp32gpioctrl" and select "esp32gpioctrl.ino"
  - In Sketch => Upload, compile and load the firmware

- Validating and testing
  - In Tools => Serial Monitor you can visualize the server dump output
    - And get the service URL
  - The red LED of the EPS32 is turn on, after 8 blinks, if the setup succeeded

- Finding the circuit IP after installation
  - The `sudo arp-scan --interface=wlan0 --localnet` command returns the corresponding IP / MAC table
    - Running the command with the circuit off and then on allows to find out its IP and MAC addresses

## Wifi configuration

The configuration of a new wifi connection is a relatively heavy process: you need to 
1. Install :
  1. The EPS32 IDE environment and
  2. The connected object firmware (e.g. [esp32gpiocontrol](https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol/), or another application such as [esp32galileo](https://gitlab.inria.fr/line/aide-group/esp32galileo) or [esp32brachistochron](https://gitlab.inria.fr/line/aide-group/esp32brachistochrone)) as described above.
2. Update the `wifi.h` file of the related firmware as explained below.
3. Reload the firmware by
  1. Opening the IDE environment and the related firmware
  2. Uploading the new elements using the `Sketch => Upload Using Programmer` menu command

### Setting the wifi essid/passwd :

- Create a `wifi.h` "secret" file
 - _cd Arduino/tools/esp32gpioctrl ; cp [wifi-configure.h](https://gitlab.inria.fr/line/aide-group/esp32gpiocontrol/-/blob/master/src/wifi-configure.h) wifi.h_
 - Edit `wifi.h` in order to set the wifi essid and password to be used, taking care of the syntax.
 - Do NOT add wifi.h to the git repository.

- To update the wifi connection (e.g. adding a new access point)
  - Edit `wifi.h` in order to set the wifi essid and password to be used, taking care of the syntax.
  - Recompile and upload using the `Sketch => Upload Using Programmer` IDE menu command

- To obtain the circuit IP the simplest method is to:
  - Scan the network (e.g., using the `sudo arp-scan --interface=wlan0 --localnet` or any other tool) _without_ and then )__with_ the circuit connected allowing to detect which new IP adress has been added.
  - Furthermore, each firmware defines an _hostaname_ which is useful to retrieve the IP address using standard tools.
 



