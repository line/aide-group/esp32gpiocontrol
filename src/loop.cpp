// Implements the ESP32 service main loop, here implementing the web service client handler
// => do not modify unless you know why and how ;)

#include <WebServer.h>

extern WebServer *server;

void setInterval_manage();

void loop()
{
  // Manages the sofware implemented event mechanism
  setInterval_manage();
  // Manages the web service client request mechanism
  server->handleClient();
}
