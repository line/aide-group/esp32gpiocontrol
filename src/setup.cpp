////////////////////////////////////////////////////////////////////////////////

//
// Here are application dependent services to be added, following the setup_gpio_service() example
//

void setup_gpio_service();

void setup_service()
{
  setup_gpio_service();
}
//
// Here are application optional dependent mechanism to be initialized just on boot
//

void setup_init()
{
  //
}
//
// This allows to define the DHCP hostname, to identify the device
//

#define HOSTNAME "esp32gpiocontrol"

////////////////////////////////////////////////////////////////////////////////

// Defines the firmware version : incremented after each noticable change
#define VERSION "0.2.8"

// Defines the server lister port
#define SERVER_LISTEN_PORT 80

////////////////////////////////////////////////////////////////////////////////

#include <WiFi.h>
#include <WebServer.h>
#include "time.h"

#include <map>
#include "wifi.h"

// Web server mechanisms
WebServer *server;
char printf_buffer[1024], mac_address[32], firmware_version[32] = VERSION;

// Setups the ESP32 service, this routine is run once by the ESP32 system

void setup()
{
  // Inits application dependent mechanism
  {
    setup_init();
  }

  // Starts the serial line dump
  {
    Serial.begin(115200);
    delay(500);
    // Skips one page
    {
      for(unsigned int n = 0; n < 20; n++) {
        Serial.println("");
      }
    }
    Serial.println("+ Serial line connected");
  }

  // Connects to the wifi network
  {
    WiFi.setHostname(HOSTNAME);
    for(auto it = wifis.begin(); it != wifis.end(); it++) {
      WiFi.begin(it->first.c_str(), it->second.c_str());
      unsigned long time_limit = millis() + 5000;
      while(WiFi.status() != WL_CONNECTED) {
        delay(500);
        if(millis() > time_limit) {
          break;
        }
      }
      if(WiFi.status() == WL_CONNECTED) {
        Serial.println("connected to '" + it->first + "' SSID");
        break;
      }
    }
    if(WiFi.status() != WL_CONNECTED) {
      Serial.println("WiFi connection timeout: Restarting the board");
      ESP.restart();
    }
    Serial.println("+ WiFi connected");
    // Retrieves the MAC address
    {
      byte mac[6];
      WiFi.macAddress(mac);
      sprintf(mac_address, "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    }
  }

  // Initializes the server and configure its URLs routing
  {
    server = new WebServer(SERVER_LISTEN_PORT);
    setup_service();
    server->begin();
    Serial.println("+ HTTP server started");
  }

  // Turns onboard LED steady on, after blinks, to indicate setup success
  {
    pinMode(13, OUTPUT);
    for(unsigned int n = 1; n < 4; n++) {
      digitalWrite(13, n % 2);
      delay(500);
    }
    Serial.println("+ LED blinking done");
  }

  // Prints the service URL
  {
    sprintf(printf_buffer, "\n=> Firmware version '%s' from MAC '%s' with hostname '%s'\n=> Listening at: http://%s:%d\n",
            firmware_version, mac_address, WiFi.getHostname(), WiFi.localIP().toString().c_str(), SERVER_LISTEN_PORT);
    Serial.println(printf_buffer);
  }
}
